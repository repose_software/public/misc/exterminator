
using System.Threading.Tasks;
using System.IO;

using DSharpPlus.Entities;
using System.Diagnostics;
using System.Net;
using System;
using Newtonsoft.Json.Linq;
using System.Globalization;

namespace Exterminator.Modules.Core {
    abstract class NoAnime {
        static public async Task OnMessage(DSharpPlus.EventArgs.MessageCreateEventArgs msg) {
            if (msg.Author.IsBot == true) {
                return;
            }

            string[] sections = msg.Message.Content.Split(" ");

            bool animeFound = false;

            for (int i = 0; i < sections.Length; i++) {
                if (animeFound != true && Uri.IsWellFormedUriString(sections[i], UriKind.Absolute) && IsImageUrl(sections[i])) {
                    animeFound = await RunAnimeDetection(msg, sections[i]);
                }
            }

            if (animeFound != true) {
                for (int i = 0; i < msg.Message.Attachments.Count; i++) {
                    if (animeFound != true) {
                        DiscordAttachment attachment = msg.Message.Attachments[i];

                        if (attachment.Width != 0 && attachment.Height != 0) {
                            animeFound = await RunAnimeDetection(msg, attachment.Url.ToString());
                        }
                    }
                }
            }

            if (animeFound != true) {
                for (int i = 0; i < msg.Message.Embeds.Count; i++) {
                    if (animeFound != true) {

                        DiscordEmbed embed = msg.Message.Embeds[i];

                        if (embed.Image.Width != 0 && embed.Image.Height != 0) {
                            animeFound = await RunAnimeDetection(msg, embed.Image.Url.ToString());
                        }
                    }
                }
            }
        }

        static async Task<bool> RunAnimeDetection(DSharpPlus.EventArgs.MessageCreateEventArgs msg, string url) {

            if (!Directory.Exists("tmp"))
                Directory.CreateDirectory("tmp");

            String id = Guid.NewGuid().ToString();

            String extension = Path.GetExtension(url);

            string tempPath = Path.Join("./tmp", $"{id}{extension}");
            string jsonPath = Path.Join("./tmp", $"{id}.json");

            using (var client = new WebClient()) {
                client.DownloadFile(url, tempPath);
            }

            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = "python";
            start.Arguments = $"./AnimeFaceDetection/main.py -i {tempPath} -o {jsonPath} " +
                "-model AnimeFaceDetection/model/res101_faster_rcnn_iter_60000.ckpt";
            start.UseShellExecute = false;
            start.RedirectStandardOutput = false;
            Process process = Process.Start(start);
            process.WaitForExit();

            string json = File.ReadAllText(jsonPath);


            JObject jObject = JObject.Parse(json);
            JToken[] jFile = jObject[tempPath].ToObject<JToken[]>();

            File.Delete(tempPath);
            File.Delete(jsonPath);

            if (jFile.Length > 0) {
                await msg.Message.DeleteAsync("anime");
                await msg.Channel.SendFileAsync("./no_anime.png", msg.Author.Mention);
                return true;
            }

            return false;
        }

        static bool IsImageUrl(string URL) {
            var req = (HttpWebRequest)HttpWebRequest.Create(URL);
            req.Method = "HEAD";
            using (var resp = req.GetResponse()) {
                return resp.ContentType.ToLower(CultureInfo.InvariantCulture)
                           .StartsWith("image/");
            }
        }
    }
}