using System;
using System.IO;

namespace Exterminator.Modules.Core {
    static class Logger {
        /* 
         * Level contains log levels which are used in the Log() function and are used to determine if it needs to be logged
         */
        public enum Level {
            VERB = 0,
            INFO = 1,
            WARN = 2,
            EXEP = 3,
        }

        public static void Log(string message, Level level) {
            Console.WriteLine($"{level.ToString()} {DateTime.Now} - {message}");
        }

        public static void Log(Exception exception) {
            Console.WriteLine($"EXEP {DateTime.Now} - {exception.Message}");
            Console.Write(exception.StackTrace + '\n');
        }
    }
}
