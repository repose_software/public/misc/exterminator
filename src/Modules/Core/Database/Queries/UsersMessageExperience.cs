using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Npgsql;
using NpgsqlTypes;

namespace Exterminator.Modules.Core.Database.Queries {
    public class UsersMessageExperience {
        public Guid? id;
        public string userId;
        public string guildId;
        public long messageCount;
        public long randomGainFactor;
        public DateTime? creationDate;
        public DateTime? lastGainDate;
        public string lastMessage;

        public UsersMessageExperience(Guid? id, string userId, string guildId, long messageCount, long randomGainFactor,
            DateTime? creationDate, DateTime? lastGainDate, string lastMessage) {
            this.id = id;
            this.userId = userId;
            this.guildId = guildId;
            this.messageCount = messageCount;
            this.randomGainFactor = randomGainFactor;
            this.creationDate = creationDate;
            this.lastGainDate = lastGainDate;
            this.lastMessage = lastMessage;
        }

        private static UsersMessageExperience Transform(Dictionary<String, dynamic> data) {
            return new UsersMessageExperience(data["id"], data["user_id"], data["guild_id"], data["message_count"],
                data["random_gain_factor"], data["creation_date"], data["last_gain_date"], data["last_message"]);
        }

        public static async Task<UsersMessageExperience> Find(string id) {
            using (NpgsqlCommand command = new NpgsqlCommand("SELECT * FROM users_message_experience WHERE id = @id", Connection.Get())) {
                command.Parameters.AddWithValue("id", NpgsqlDbType.Uuid, id);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("UsersMessageExperience: Not found");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task<List<UsersMessageExperience>> FindByUser(string userId) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                SELECT * FROM users_message_experience
                    WHERE user_id = @user_id
            ", Connection.Get())) {
                command.Parameters.AddWithValue("user_id", NpgsqlDbType.Text, userId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    List<UsersMessageExperience> data = new List<UsersMessageExperience>();

                    while (await result.ReadAsync()) {
                        dynamic entry = new Dictionary<string, dynamic>();

                        for (int i = 0; i < result.FieldCount; i++) {
                            Type type = result.GetFieldType(i);
                            entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                        }

                        data.Add(Transform(entry));
                    }

                    return data;
                }
            }
        }

        public static async Task<UsersMessageExperience> Insert(UsersMessageExperience data) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                INSERT INTO users_message_experience 
                    (user_id, guild_id, message_count, random_gain_factor, last_message) VALUES 
                    (@user_id, @guild_id, @message_count, @random_gain_factor, @last_message) 
                RETURNING *
            ", Connection.Get())) {
                command.Parameters.AddWithValue("user_id", NpgsqlDbType.Text, data.userId);
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, data.guildId);
                command.Parameters.AddWithValue("message_count", NpgsqlDbType.Integer, data.messageCount);
                command.Parameters.AddWithValue("random_gain_factor", NpgsqlDbType.Integer, data.randomGainFactor);
                command.Parameters.AddWithValue("last_message", NpgsqlDbType.Text, data.lastMessage);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("UsersMessageExperience: Failed to insert");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task<UsersMessageExperience> Update(UsersMessageExperience data) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                UPDATE users_message_experience SET 
                    (user_id, guild_id, message_count, random_gain_factor, last_gain_date, creation_date, last_message) = 
                    (@user_id, @guild_id, @message_count, @random_gain_factor, @last_gain_date, @creation_date, @last_message) 
                WHERE id = @id RETURNING *
            ", Connection.Get())) {
                command.Parameters.AddWithValue("id", NpgsqlDbType.Uuid, data.id);
                command.Parameters.AddWithValue("user_id", NpgsqlDbType.Text, data.userId);
                command.Parameters.AddWithValue("guild_id", NpgsqlDbType.Text, data.guildId);
                command.Parameters.AddWithValue("message_count", NpgsqlDbType.Integer, data.messageCount);
                command.Parameters.AddWithValue("random_gain_factor", NpgsqlDbType.Integer, data.randomGainFactor);
                command.Parameters.AddWithValue("creation_date", NpgsqlDbType.Timestamp, data.creationDate);
                command.Parameters.AddWithValue("last_gain_date", NpgsqlDbType.Timestamp, data.lastGainDate);
                command.Parameters.AddWithValue("last_message", NpgsqlDbType.Text, data.lastMessage);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("UsersMessageExperience: Failed to update");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }
    }
}