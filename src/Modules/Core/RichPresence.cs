
using System.Threading.Tasks;
using System.IO;

using DSharpPlus.Entities;
using System.Diagnostics;
using System.Net;
using System;
using Newtonsoft.Json.Linq;
using System.Globalization;
using DSharpPlus;

namespace Exterminator.Modules.Core {
    abstract class RichPresence {
        static DiscordClient _client;

        static public void SetClient(DiscordClient client) {
            _client = client;
        }

        static public async Task update(DSharpPlus.EventArgs.HeartbeatEventArgs _) {
            int guilds = _client.Guilds.Count;
            int users = 0;

            foreach (var guild in _client.Guilds.Values) {
                users += guild.MemberCount;
            }

            if (users > 0) {
                await _client.UpdateStatusAsync(new DiscordGame($"with {users} slaves"));
            }
        }
    }
}