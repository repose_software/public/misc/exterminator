using System.Threading.Tasks;
using System.Linq;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

using Exterminator.Modules.Core;
using Exterminator.Modules.Commands;
using Exterminator.Modules.AutoRole.Queries;

namespace Exterminator.Modules.AutoRole.Commands {

    [Group("autorole")]
    public class AutoRole : BaseCommand {
        [Command("add")]
        [RequirePermissions(DSharpPlus.Permissions.Administrator)]
        public async Task Add(CommandContext ctx, DiscordRole role = null) {
            if (role == null) {
                await ctx.RespondAsync($"{ctx.Member.Mention} I need a role to add, is that so difficult?");
                return;
            }

            var autorole = new AutoRoles(null, ctx.Guild.Id.ToString(), role.Id.ToString(), null);
            await AutoRoles.Insert(autorole);
            await ctx.RespondAsync($"{ctx.Member.Mention} Added the autorole.");
        }

        [Command("remove")]
        [RequirePermissions(DSharpPlus.Permissions.Administrator)]
        public async Task Remove(CommandContext ctx, DiscordRole role = null) {
            if (role == null) {
                await ctx.RespondAsync($"{ctx.Member.Mention} I need a role to remove, is that so difficult?");
                return;
            }

            await AutoRoles.Delete(ctx.Guild.Id.ToString(), role.Id.ToString());
            await ctx.RespondAsync($"{ctx.Member.Mention} Removed the autorole if it was there.");
        }

        [Command("list")]
        [RequirePermissions(DSharpPlus.Permissions.Administrator)]
        public async Task List(CommandContext ctx) {
            var embed = new DiscordEmbedBuilder();
            embed = embed.WithColor(DiscordColor.Orange);
            embed = embed.WithAuthor($"Autoroles for {ctx.Guild.Name}", null, ctx.Client.CurrentUser.AvatarUrl);
            embed = embed.WithFooter("\u00a9 TAYO Creative — Built for the lulz");
            embed = embed.WithDescription("These roles are given **automatically** given when a user joins the guild.");

            var autoroles = await AutoRoles.FindByGuild(ctx.Guild.Id.ToString());

            foreach (var role in autoroles) {
                DiscordRole discordRole = null;

                try {
                    discordRole = ctx.Guild.Roles.First(x => x.Id.ToString() == role.roleId);
                } catch (System.Exception) {
                    await AutoRoles.Delete(role.guildId, role.roleId);
                    continue;
                }

                if (discordRole != null) {
                    embed = embed.AddField(discordRole.Name, $"Role ID: {role.roleId}", true);
                }
            }

            await ctx.RespondAsync(null, false, embed);
        }

        /* 
         * Registers this class with all its commands for use in the bot
         */
        override public void Register() {
            Program.CommandsNext.RegisterCommands<AutoRole>();
            Logger.Log("Set up auto roles module!", Logger.Level.INFO);
        }
    }
}