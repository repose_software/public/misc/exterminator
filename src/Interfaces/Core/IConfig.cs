using Exterminator.Interfaces.Core.Database;

namespace Exterminator.Interfaces.Core {
    public interface IConfig {
        IDatabase postgresql { get; set; }
        string discordToken { get; set; }
        string commandPrefix { get; set; }
    }
}