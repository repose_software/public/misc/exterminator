using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Npgsql;
using NpgsqlTypes;

namespace Exterminator.Modules.Core.Database.Queries {
    public class UsersGlobalExperience {
        public Guid? id;
        public string userId;
        public long xp;
        public long xpTowardsNext;
        public long level;
        public DateTime? creationDate;
        public long? rank;

        public UsersGlobalExperience(Guid? id, string userId, long xp, long xpTowardsNext,
            long level, DateTime? creationDate, long? rank) {
            this.id = id;
            this.userId = userId;
            this.xp = xp;
            this.xpTowardsNext = xpTowardsNext;
            this.level = level;
            this.creationDate = creationDate;
            this.rank = rank;
        }

        private static UsersGlobalExperience Transform(Dictionary<String, dynamic> data) {
            return new UsersGlobalExperience(data["id"], data["user_id"], data["xp"],
                data["xp_towards_next"], data["level"], data["creation_date"], data.ContainsKey("rank") ? data["rank"] : null);
        }

        public static async Task<UsersGlobalExperience> Find(string id) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                SELECT * FROM (
                    SELECT users_global_experience.*,
                            row_number() OVER (ORDER BY xp DESC) as rank
                            FROM users_global_experience
                ) WHERE id = @id
            ", Connection.Get())) {
                command.Parameters.AddWithValue("id", NpgsqlDbType.Uuid, id);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("UsersGlobalExperience: Not found");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task<List<UsersGlobalExperience>> SelectLimit(int limit, int offset) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                SELECT users_global_experience.*,
                    row_number() OVER (ORDER BY xp DESC) as rank
                    FROM users_global_experience
                    LIMIT @limit
                    OFFSET @offset
            ", Connection.Get())) {
                command.Parameters.AddWithValue("limit", NpgsqlDbType.Integer, limit);
                command.Parameters.AddWithValue("offset", NpgsqlDbType.Integer, offset);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    List<UsersGlobalExperience> data = new List<UsersGlobalExperience>();

                    while (await result.ReadAsync()) {
                        dynamic entry = new Dictionary<string, dynamic>();

                        for (int i = 0; i < result.FieldCount; i++) {
                            Type type = result.GetFieldType(i);
                            entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                        }

                        data.Add(Transform(entry));
                    }

                    return data;
                }
            }
        }

        public static async Task<List<UsersGlobalExperience>> FindByUser(string userId) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                SELECT * FROM (
                    SELECT users_global_experience.*,
                            row_number() OVER (ORDER BY xp DESC) as rank
                            FROM users_global_experience
                ) ORDER BY rank ASC WHERE user_id = @user_id
            ", Connection.Get())) {
                command.Parameters.AddWithValue("user_id", NpgsqlDbType.Text, userId);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    List<UsersGlobalExperience> data = new List<UsersGlobalExperience>();

                    while (await result.ReadAsync()) {
                        dynamic entry = new Dictionary<string, dynamic>();

                        for (int i = 0; i < result.FieldCount; i++) {
                            Type type = result.GetFieldType(i);
                            entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                        }

                        data.Add(Transform(entry));
                    }

                    return data;
                }
            }
        }

        public static async Task<UsersGlobalExperience> Insert(UsersGlobalExperience data) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                INSERT INTO users_global_experience 
                    (user_id, xp, xp_towards_next, level) VALUES 
                    (@user_id, @xp, @xp_towards_next, @level) 
                RETURNING *
            ", Connection.Get())) {
                command.Parameters.AddWithValue("user_id", NpgsqlDbType.Text, data.userId);
                command.Parameters.AddWithValue("xp", NpgsqlDbType.Integer, data.xp);
                command.Parameters.AddWithValue("xp_towards_next", NpgsqlDbType.Integer, data.xpTowardsNext);
                command.Parameters.AddWithValue("level", NpgsqlDbType.Integer, data.level);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("UsersGlobalExperience: Failed to insert");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }

        public static async Task<UsersGlobalExperience> Update(UsersGlobalExperience data) {
            using (NpgsqlCommand command = new NpgsqlCommand($@"
                UPDATE users_global_experience SET 
                    (user_id, xp, xp_towards_next, level) = 
                    (@user_id, @xp, @xp_towards_next, @level) 
                WHERE id = @id RETURNING *
            ", Connection.Get())) {
                command.Parameters.AddWithValue("id", NpgsqlDbType.Uuid, data.id);
                command.Parameters.AddWithValue("user_id", NpgsqlDbType.Text, data.userId);
                command.Parameters.AddWithValue("xp", NpgsqlDbType.Integer, data.xp);
                command.Parameters.AddWithValue("xp_towards_next", NpgsqlDbType.Integer, data.xpTowardsNext);
                command.Parameters.AddWithValue("level", NpgsqlDbType.Integer, data.level);
                command.Prepare();
                using (System.Data.Common.DbDataReader result = await command.ExecuteReaderAsync()) {
                    if (result.HasRows == false) {
                        throw new Exception("UsersGlobalExperience: Failed to update");
                    }

                    await result.ReadAsync();

                    dynamic entry = new Dictionary<string, dynamic>();

                    for (int i = 0; i < result.FieldCount; i++) {
                        Type type = result.GetFieldType(i);
                        entry[result.GetName(i)] = Convert.ChangeType(result.GetFieldValue<Object>(i), type);
                    }

                    return Transform(entry);
                }
            }
        }
    }
}